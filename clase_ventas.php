<?php 
session_start(); 

if (isset($_SESSION['K_username'])) 
{
}
else
{ 
header('location: index.php');	
}
?>

<?php

require_once 'clasecv.php';
require_once 'clase_cliente.php';
require_once 'clase_producto.php';

class venta{

var	$codigoventa;
var $cedula_cliente;
var $codproducto;
var	$fechaventa;
var	$precio;
var $descuentos;
var $cantidad;

 function _construct($codigoventa1="",$cedula_cliente1="",$codproducto1="",$fechaventa1="",$precio1=0,$descuentos1="",$cantidad1=0)      
 {
 	$this->codigoventa=$codigoventa1;
	$this->cedula_cliente=$cedula_cliente1;
	$this->codproducto=$codproducto1;
	$this->fechaventa=$fechaventa1;
	$this->precio=$precio1;
    $this->descuentos=$descuentos1;
    $this->cantidad=$cantidad1;
	}
  
function insertar1()
{
	$con1= new basedatos();	
	$sql_select1 = "insert into ventas(codigo_venta,valor,fecha_venta,codigo_cliente,descuento) values('$this->codigoventa','$this->precio','$this->fechaventa','$this->cedula_cliente','$this->descuentos')";

	$query1 = $con1->ejecutar($sql_select1);
	if (!$query1){
		header('location:ventas.php?mensaje=18');
		$client= new cliente();
		$client->_construct($this->cedula_cliente," "," "," "," "," ");
		if ($client->buscar(2)==0)
		{
		header('location:ventas.php?mensaje=1');
		}
		  }
		 else{
		 $this->actualiza_precio($this->codigoventa);
		 header('location:ventas.php?mensaje=2');
		}	
	}
	
	
	function insertar2()
{
	$con1= new basedatos();	
	$sql_select2 = "insert into venta_producto(codigo_ventas,codigo_producto,cantidad) values('$this->codigoventa','$this->codproducto','$this->cantidad')";
	
	$query2 = $con1->ejecutar($sql_select2);
	if (!$query2)
	
	{
		header('location:ventas.php?mensaje=18');
		$produc= new producto();
		$produc->_construct($this->codproducto," ",0,0," ",0);
		if ($produc->buscar(2)==0)
		{
		 header('location:ventas.php?mensaje=5');
		}
	}
				 else{
		$produc= new producto();
		$produc->_construct($this->codproducto," ",0,0," ",0);
		$produc->actualiza_cantidad();
		$this->modifica_precio($this->codigoventa,$this->codproducto);
		header('location:ventas.php?mensaje=2');
		}
	}
	

function buscar($opcion)
{
	$con1= new basedatos();	
	if($opcion==1)
	{
	$query = "select * from ventas where codigo_venta='$this->codigoventa'";		
	}
	
	if($opcion==2)
	{
	$query = "select * from venta_producto where codigo_producto='$this->codproducto' and codigo_ventas='$this->codigoventa'";
	}	
	$consulta1 = $con1->ejecutar($query);
		
	$n_elementos = pg_num_rows($consulta1);
	
	
	  return $n_elementos;
	  
}

function eliminar_producto_venta()
{
	$con1= new basedatos();	
	$produc= new producto();
	$query1 = "delete from venta_producto where codigo_producto='$this->codproducto' and codigo_ventas='$this->codigoventa'";
	$consulta1 = $con1->ejecutar($query1);	
	$this->actualiza_precio($this->codigoventa);
	$produc->_construct($this->codproducto," ",0,0," ",0);
	$produc->actualiza_cantidad();
	
		
	}
	
	function eliminar_ventas()
{
	$con1= new basedatos();
	$query3 = "select codigo_producto from venta_producto where codigo_ventas='$this->codigoventa'";
	$query1 = "delete from venta_producto where codigo_ventas='$this->codigoventa'";
	$query2 = "delete from ventas where codigo_venta='$this->codigoventa'";
	
	$consulta3 = $con1->ejecutar($query3);
	$consulta1 = $con1->ejecutar($query1);	
	$consulta2 = $con1->ejecutar($query2);
	
	$n_elementos = pg_num_rows($consulta3);
	$produc= new producto();
	
	for($i=0; $i<$n_elementos; $i++) {
	$codi = pg_result($consulta3, $i, "codigo_producto");
	$produc->_construct($codi," ",0,0," ",0);
	$produc->actualiza_cantidad();
	}

	}

function actualiza_venta()
{
	$con1= new basedatos();	
	if($this->cedula_cliente!=" ")
	{
	$query = "update ventas SET codigo_cliente='$this->cedula_cliente' where codigo_venta='$this->codigoventa'";
	$consulta1 = $con1->ejecutar($query); 
	}	
	if($this->fechaventa!=" ")
	{
	$query = "update ventas SET fecha_venta='$this->fechaventa' where codigo_venta='$this->codigoventa'";
	$consulta1 = $con1->ejecutar($query); 
	}
}


function actualiza_producto_venta()
{	
	$con1= new basedatos();
	if($this->descuentos!=" ")
	{
	$query = "update venta_producto SET descuentos='$this->descuentos' where codigo_ventas='$this->codigoventa' and codigo_producto='$this->codproducto'";
	$consulta1 = $con1->ejecutar($query); 
	}	
	if($this->cantidad!=0)
	{
	$query = "update venta_producto SET cantidad='$this->cantidad' where codigo_ventas='$this->codigoventa' and codigo_producto='$this->codproducto'";
	$consulta1 = $con1->ejecutar($query); 
	$this->actualiza_precio($this->codigoventa);
	}
	$produc= new producto();
	$produc->_construct($this->codproducto," ",0,0," ",0);
	$produc->actualiza_cantidad();
  }

function actualiza_precio($codi_venta)
{
		$con3= new basedatos();
		$query2 = "select codigo_producto from venta_producto where codigo_ventas='$codi_venta'";
		$consulta2 = $con3->ejecutar($query2);
		$query1 = "select descuento from ventas where codigo_venta='$codi_venta'";
		$consulta1 = $con3->ejecutar($query1);
		$desc = pg_result($consulta1, 0, "descuento");
		$query = "update ventas SET valor=-$desc where codigo_venta='$codi_venta'";
		$con3->ejecutar($query); 
			$n_elementos1 = pg_num_rows($consulta2);
			echo $n_elementos1;
			for($j=0; $j<$n_elementos1; $j++) {
			$codi_produ = pg_result($consulta2, $j, "codigo_producto");
			echo $codi_produ;
			$this->modifica_precio($codi_venta, $codi_produ);
			}

}

function modifica_precio($codiven, $codiprodu)
{
		$con3= new basedatos();	
		$query1 = "select precio_venta from productos where codigo='$codiprodu'";
		$query2 = "select valor from ventas where codigo_venta='$codiven'";
		$query3 = "select cantidad from venta_producto where codigo_producto='$codiprodu' and codigo_ventas='$codiven'";
		$consulta1= $con3->ejecutar($query1);
		$consulta2 = $con3->ejecutar($query2);
		$consulta3 = $con3->ejecutar($query3);
		$canti = pg_result($consulta3, 0, "cantidad");
		$this->precio = pg_result($consulta1, 0, "precio_venta");
		$valor1 = pg_result($consulta2, 0, "valor");
		$query = "update ventas SET valor=$valor1+$canti*'$this->precio' where codigo_venta='$codiven'";
		$con3->ejecutar($query);  
}
}
?>